import tkinter.ttk as ttk
from tkinter import *
from MainMenuWindow import MainMenuWindow


class LoginWindow(ttk.Frame):

    def __init__(self, master=None):
        ttk.Frame.__init__(self, master)

        self.master = master
        self.master.geometry('800x600')  # Rozmiar okna
        self.master.title("MSRD")
        self.pack(fill=BOTH, expand=True)
        self.layout()

    def layout(self):
        title = ttk.Label(self, text="Program MSRD", style="title.TLabel")
        title.place(x=0, y=30)

        l1 = ttk.Label(self, text="Proszę wprowadzić dlugości wlotów", style="txt.TLabel")
        l1.place(x=10, y=110)

        l2 = ttk.Label(self, text="Wlot nr 1:", style="txt.TLabel")
        l2.place(x=10, y=150)

        l3 = ttk.Label(self, text="Wlot nr 2:", style="txt.TLabel")
        l3.place(x=10, y=200)

        l4 = ttk.Label(self, text="Wlot nr 3:", style="txt.TLabel")
        l4.place(x=10, y=250)

        l5 = ttk.Label(self, text="Wlot nr 4:", style="txt.TLabel")
        l5.place(x=10, y=300)

        l6 = ttk.Label(self, text="m", style="txt.TLabel")
        l6.place(x=180, y=150)

        l7 = ttk.Label(self, text="m", style="txt.TLabel")
        l7.place(x=180, y=200)

        l8 = ttk.Label(self, text="m", style="txt.TLabel")
        l8.place(x=180, y=250)

        l9 = ttk.Label(self, text="m", style="txt.TLabel")
        l9.place(x=180, y=300)

        e1 = ttk.Entry(self, width=10)
        e1.place(x=110, y=150)

        e2 = ttk.Entry(self, width=10)
        e2.place(x=110, y=200)

        e3 = ttk.Entry(self, width=10)
        e3.place(x=110, y=250)

        e4 = ttk.Entry(self, width=10)
        e4.place(x=110, y=300)

        b1 = ttk.Button(self, text="OK", command=self.handleOk)
        b1.place(x=150, y=400)

        b2 = ttk.Button(self, text="Zakończ", command=self.handleClose)
        b2.place(x=250, y=400)

        # ----------- STYLES ------------

        style = ttk.Style()

        style.configure("TFrame",
                        background="#607D8B"
                        )
        style.configure("title.TLabel",
                        background="#455A64",
                        foreground="#42A5F5",
                        font="Arial 20 bold italic",
                        width=500,
                        padding=10
                        )
        style.configure("txt.TLabel",
                        background="#607D8B",
                        foreground="#B0BEC5",
                        font="Arial 12",
                        )
        style.configure("TButton",
                        background="#607D8B",
                        foreground="#455A64",
                        font="Arial 9 bold"
                        )
        style.configure("TCheckbutton",
                        background="#607D8B",
                        foreground="#B0BEC5",
                        font="Arial 12"
                        )
        style.configure("warning.TLabel",
                        background="#607D8B",
                        foreground="#B0BEC5",
                        font="Arial 10 bold",
                        )
        style.configure("status.TLabel",
                        background="#607D8B",
                        foreground="#B0BEC5",
                        font="Arial 12",
                        )

    # ----------- EVENTS ------------

    def handleOk(self):
        root = Tk()
        MainMenuWindow(root)
        root.mainloop()
        self.master.destroy()

    def handleClose(self):
        self.master.destroy()
