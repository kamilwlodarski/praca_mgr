from tkinter import *
import tkinter.ttk as ttk
import numpy as np
import matplotlib.pyplot as plt

#plt.axis([0, 10, 0, 1])

# evenly sampled time at 200ms intervals
t = np.arange(0., 5., 0.2)

# red dashes, blue squares and green triangles
line = plt.plot(t, t**2)
line1 = plt.plot(t, t**3 )
plt.xlabel('t [s]')
plt.ylabel('v [km/h]')
plt.title('Wykresy prędkości pojazdów \ndla wlotu nr 1')
plt.show()
