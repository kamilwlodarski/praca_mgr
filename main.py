import tkinter.ttk as ttk
from tkinter import Tk


from AnimationWindow import AnimationWindow
class Main(Tk.frame):

    def main():
        root = Tk()
        app = AnimationWindow(root)
        root.mainloop()

    if __name__ == "__main__":
        main()
