import tkinter.ttk as ttk
from tkinter import *


class MainMenuWindow(ttk.Frame):

    def __init__(self, master=None):
        ttk.Frame.__init__(self, master)

        self.master = master
        self.master.geometry('800x600')  # Rozmiar okna
        self.master.title("MSRD")

        self.pack(fill=BOTH, expand=True)

        self.layout()

    def layout(self):
        title = ttk.Label(self, text="Program MSRD", style="title.TLabel")
        title.place(x=0, y=30)

        l1 = ttk.Label(self, text="Menu główne", style="subtitle.TLabel")
        l1.place(x=10, y=110)

        l2 = ttk.Label(self, text="Wybierz jedną opcję w celu wygenerowania:", style="txt.TLabel")
        l2.place(x=10, y=160)

        b1 = ttk.Button(self,
                        text="                 Numer listy (systemowy), dla której \n będzie drukowany na monitorze wykres droga-czas",
                        style="TButton", width=50)
        b1.place(x=50, y=210)

        b2 = ttk.Button(self, text="Struktury rodzajowej i liczby pojazdów \n                     na skrzyżowaniu",
                        style="TButton", width=50)
        b2.place(x=430, y=210)

        b3 = ttk.Button(self, text="Struktury kierunkowej i kolejek pojazdów\n ",
                        style="TButton", width=50)
        b3.place(x=50, y=270)

        b4 = ttk.Button(self, text="Czasów podróży bez opóźnienia i rzeczywistych\n ",
                        style="TButton", width=50)
        b4.place(x=430, y=270)

        b5 = ttk.Button(self,
                        text="Wielkości kontrolnych w każdym kroku symulacyjnym \n              oraz przebiegu symulacji na monitorze",
                        style="TButton", width=50)
        b5.place(x=50, y=330)

        b6 = ttk.Button(self,
                        text="Opóźnienia systemowego i opóźnienia przy zatrzymaniu\n ",
                        style="TButton", width=50)
        b6.place(x=430, y=330)

        b7 = ttk.Button(self,
                        text="Prędkości podróży bez opóźnienia i rzeczywistej\n ",
                        style="TButton", width=50)
        b7.place(x=50, y=390)

        b8 = ttk.Button(self,
                        text="Prędkości chwilowych na wejściu i na wyjściu\n ",
                        style="TButton", width=50)
        b8.place(x=430, y=390)

        b9 = ttk.Button(self,
                        text="Prędkości chwilowych na linii wjazdu na skryżowanie\n ",
                        style="TButton", width=50)
        b9.place(x=50, y=450)

        b10 = ttk.Button(self,
                         text="Wykresy prędkości\n ",
                         style="TButton", width=50)
        b10.place(x=430, y=450)

        b11 = ttk.Button(self, text="OK", style="TButton")
        b11.place(x=250, y=540)

        b12 = ttk.Button(self, text="Zakończ", style="TButton", command=self.handleClose)
        b12.place(x=450, y=540)

        # ----------- STYLES ------------

        style = ttk.Style()

        style.configure("TFrame",
                        background="#607D8B"
                        )
        style.configure("title.TLabel",
                        background="#455A64",
                        foreground="#42A5F5",
                        font="Arial 20 bold italic",
                        width=500,
                        padding=10
                        )
        style.configure("subtitle.TLabel",
                        background="#607D8B",
                        foreground="#B0BEC5",
                        font="Arial 14 bold",
                        )
        style.configure("txt.TLabel",
                        background="#607D8B",
                        foreground="#B0BEC5",
                        font="Arial 12",
                        )
        style.configure("TButton",
                        background="#607D8B",
                        foreground="#455A64",
                        font="Arial 9 bold",
                        compound=CENTER,
                        )
        style.configure("warning.TLabel",
                        background="#607D8B",
                        foreground="#B0BEC5",
                        font="Arial 10 bold",
                        )
        style.configure("status.TLabel",
                        background="#607D8B",
                        foreground="#B0BEC5",
                        font="Arial 12",
                        )
        # -------------- EVENTS --------------

    def handleClose(self):
        self.master.destroy()
