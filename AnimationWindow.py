import tkinter.ttk as ttk
from tkinter import *


class AnimationWindow(ttk.Frame):
    def __init__(self, master=None):
        ttk.Frame.__init__(self, master)

        self.master = master
        self.master.geometry('1600x1200')  # Rozmiar okna
        self.master.title("MSRD")

        self.pack(fill=BOTH, expand=True)

        self.layout()
        self.drawing()

    # self.drawing()

    def layout(self):
        title = ttk.Label(self, text="Program MSRD", style="title.TLabel")
        title.place(x=0, y=30)

        l1 = ttk.Label(self, text="Menu główne", style="subtitle.TLabel")
        l1.place(x=10, y=110)

        l2 = ttk.Label(self, text="Wybierz jedną opcję w celu wygenerowania:", style="txt.TLabel")
        l2.place(x=10, y=160)

        b1 = ttk.Button(self, text="Zakończ", style="TButton")
        b1.place(x=100, y=1100)

    def drawing(self):
        canvas = Canvas(self, width=1100, height=1100)
        canvas.pack()
        canvas.create_rectangle(50, 50, 950, 950, fill="#e3e5e2", outline="")
        canvas.create_line(400, 150, 400, 400)
        canvas.create_line(100, 400, 400, 400)
        canvas.create_line(400, 600, 400, 850)
        canvas.create_line(100, 600, 400, 600)
        canvas.create_line(600, 150, 600, 400)
        canvas.create_line(600, 400, 900, 400)
        canvas.create_line(600, 600, 600, 850)
        canvas.create_line(600, 600, 900, 600)
        canvas.create_arc(350, 400, 400, 350, extent="-90")
        canvas.create_arc(650, 600, 600, 650, extent="-90")



    # ----------- STYLES ------------

    style = ttk.Style()

    style.configure("TFrame",
                    background="#607D8B"
                    )
    style.configure("title.TLabel",
                    background="#455A64",
                    foreground="#42A5F5",
                    font="Arial 20 bold italic",
                    width=500,
                    padding=10
                    )
    style.configure("subtitle.TLabel",
                    background="#607D8B",
                    foreground="#B0BEC5",
                    font="Arial 14 bold",
                    )
    style.configure("txt.TLabel",
                    background="#607D8B",
                    foreground="#B0BEC5",
                    font="Arial 12",
                    )
    style.configure("TButton",
                    background="#607D8B",
                    foreground="#455A64",
                    font="Arial 9 bold",
                    compound=CENTER,
                    )
    style.configure("warning.TLabel",
                    background="#607D8B",
                    foreground="#B0BEC5",
                    font="Arial 10 bold",
                    )
    style.configure("status.TLabel",
                    background="#607D8B",
                    foreground="#B0BEC5",
                    font="Arial 12",
                    )
